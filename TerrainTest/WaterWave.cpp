//
//  WaterWave.cpp
//  TerrainTest
//
//  Created by Jeon ByungSoo on 13. 6. 7..
//
//

#include "WaterWave.h"

using namespace cocos2d;
using namespace std;

/**
 -Ideas to make it better:
 * Add clamping to avoid giant, crappy looking waves.
 * Make it run on a fixed timestep.
 * Add texturing to give the surface a nice (and anti-aliased) look.
 * Fancier makeSplash method that does more than a single point.
 */

WaterWave::WaterWave()
:m_bounds(CCRectZero),
m_damping(0),
m_diffusion(0),
m_nVerts(0),
m_h1(),
m_h2(),
m_texCoords(),
m_texCoords2(),
m_surfaceVerts(),
m_terrain(NULL),
m_terrain2(NULL),
m_program(NULL),
m_texture0ID(0),
m_texture1ID(0),
m_cumulatedT(0)
{
    m_h1.clear();
    m_h2.clear();
}

WaterWave::~WaterWave()
{
    delete m_program;
}

WaterWave* WaterWave::create(CCRect bounds, int nVerts, float damping, float diffusion)
{
    WaterWave *pRet = new WaterWave();
    if (pRet && pRet->initWithParams(bounds, nVerts, damping, diffusion)) {
        pRet->autorelease();
    } else {
        CC_SAFE_DELETE(pRet);
    }
    return pRet;
}

bool WaterWave::initWithParams(CCRect bounds, int nVerts, float damping, float diffusion)
{
    if (!CCNode::init()) {
        return false;
    }
    
    m_bounds = bounds;
    m_nVerts = nVerts;
    m_damping = damping;
    m_diffusion = diffusion;
    
    m_h1.assign(m_nVerts, 0);
    m_h2.assign(m_nVerts, 0);
    
    m_texCoords.assign(m_nVerts*2, vertex2(0, 0));
    m_texCoords2.assign(m_nVerts*2, vertex2(0, 0));
    m_surfaceVerts.assign(m_nVerts, vertex2(0, 0));
    
    // Add Terrain Texture
    m_terrain = CCSprite::create("foreground_5.jpg");
    m_terrain->retain();
    
    m_terrain2 = CCSprite::create("ghost_indicator.png");
    m_terrain2->retain();
    
    ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
    m_terrain->getTexture()->setTexParameters(&tp);
    m_terrain2->getTexture()->setTexParameters(&tp);
    
    const std::string fileP = CCFileUtils::sharedFileUtils()->fullPathForFilename("MultTexVert.vsh");
    CCString *string = CCString::createWithContentsOfFile(fileP.c_str());
    const GLchar * vertSource = string->getCString();
    
    const std::string fileP2 = CCFileUtils::sharedFileUtils()->fullPathForFilename("MultTexFrag.fsh");
    CCString *string2 = CCString::createWithContentsOfFile(fileP2.c_str());
    const GLchar * fragSource = string2->getCString();
    
    m_program = new CCGLProgram();
    m_program->initWithVertexShaderByteArray(vertSource, fragSource);
    m_program->link();
    m_program->updateUniforms();
    
    m_program->addAttribute(kVertexPosition, kVertexPosition_Idx);
    m_program->addAttribute(kTexCoord0, kTexCoord0_Idx);
    m_program->addAttribute(kTexCoord1, kTexCoord1_Idx);
    
    m_texture0ID = m_program->getUniformLocationForName(kTexSample0);
    m_texture1ID = m_program->getUniformLocationForName(kTexSample1);
    
    m_texCoords2 = m_texCoords;
    
    CCLog("%d %d", m_texture0ID, m_texture1ID);
    CCLog("%d %d %d", kVertexPosition_Idx, kTexCoord0_Idx, kTexCoord1_Idx);
    
    this->scheduleUpdate();
    
    return true;
}

void WaterWave::vertlet()
{
    for (int i=0; i<m_nVerts; ++i) m_h1[i] = 2.0f*m_h2[i] - m_h1[i];
    m_h1.swap(m_h2);
}

inline float WaterWave::diffuse(float diff, float damp, float prev, float curr, float next)
{
    return (curr*diff + ((prev + next)*0.5f)*(1.0f - diff)) * damp;
}

void WaterWave::diffuse()
{
    float prev = m_h2[0];
    float curr = m_h2[0];
    float next = m_h2[1];
    
    m_h2[0] = diffuse(m_diffusion, m_damping, prev, curr, next);
    
    for (int i=1; i<(m_nVerts - 1); ++i) {
        prev = curr;
        curr = next;
        next = m_h2[i+1];
        m_h2[i] = diffuse(m_diffusion, m_damping, prev, curr, next);
    }
    
    prev = curr;
    curr = next;
    m_h2[m_nVerts-1] = diffuse(m_diffusion, m_damping, prev, curr, next);
}

float WaterWave::dx()
{
    return (m_bounds.size.width/(GLfloat)(m_nVerts-1));
}

void WaterWave::makeSplashAt(float x)
{
    float dx = this->dx();
    int index = MAX(0, MIN((int)(x/dx), m_nVerts-1));
    m_h2[index] += CCRANDOM_MINUS1_1()*20.0f;
}

void WaterWave::updateTexCoords2()
{
    for (int i=0; i<m_texCoords2.size(); ++i) {
        m_texCoords2[i].y += m_cumulatedT;
    }
//    CCLog("%f", m_cumulatedT);
}

void WaterWave::update(float dt)
{
    if (m_cumulatedT==0) this->makeSplashAt(470.0f);
    m_cumulatedT += dt;
}

vector<ccVertex2F>* WaterWave::getSurfaceVec()
{
    return &m_surfaceVerts;
}

void WaterWave::draw()
{
    glClearColor(1.0f, 1.0f, 0, 1.0f);
    vertlet();
    diffuse();
    
    GLfloat dx = this->dx();
    GLfloat top = m_bounds.size.height;
    
    ccVertex2F verts[m_nVerts*2];
    for (int i=0; i<m_nVerts; ++i) {
        GLfloat x = i*dx;
        verts[2*i+0] = vertex2(x, 0);
        verts[2*i+1] = vertex2(x, top + m_h2[i]);
        m_surfaceVerts[i] = verts[2*i+1];
        
        float texX1 = x/TextureSize;
        m_texCoords[2*i+0] = vertex2(texX1, 1.0);
        m_texCoords[2*i+1] = vertex2(texX1, 1.0 - verts[2*i+1].y/TextureSize);
        m_texCoords2[2*i+0] = vertex2(texX1, 1.0);
        m_texCoords2[2*i+1] = vertex2(texX1, 1.0 - verts[2*i+1].y/TextureSize);

    }

    
    /**
     Ported from HelloWorldScene
     **/
    
    setShaderProgram(m_program);
    getShaderProgram()->use();

    CC_NODE_DRAW_SETUP();

    updateTexCoords2();

//    ccGLBlendFunc(CC_BLEND_SRC, GL_ZERO);

    glEnableVertexAttribArray(kVertexPosition_Idx);
    glEnableVertexAttribArray(kTexCoord0_Idx);
    glEnableVertexAttribArray(kTexCoord1_Idx);

//    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_terrain->getTexture()->getName());
    glUniform1i(m_texture0ID, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_terrain2->getTexture()->getName());
    glUniform1i(m_texture1ID, 1);

    glVertexAttribPointer(kVertexPosition_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &verts[0]);
    glVertexAttribPointer(kTexCoord0_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &m_texCoords[0]);
    glVertexAttribPointer(kTexCoord1_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &m_texCoords2[0]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei) m_nVerts*2);

    CC_INCREMENT_GL_DRAWS(1);
}

