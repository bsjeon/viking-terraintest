//
//  Constant.h
//  TerrainTest
//
//  Created by Jeon ByungSoo on 13. 6. 7..
//
//

#ifndef TerrainTest_Constant_h
#define TerrainTest_Constant_h

#define SCREEN_W CCDirector::sharedDirector()->getWinSize().width
#define SCREEN_H CCDirector::sharedDirector()->getWinSize().height

#endif
