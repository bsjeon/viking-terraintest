attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec2 b_texCoord;

varying vec2 va_texCoord;
varying vec2 vb_texCoord;

void main()
{
    gl_Position = CC_MVPMatrix * a_position;
    va_texCoord = a_texCoord;
    vb_texCoord = b_texCoord;
}