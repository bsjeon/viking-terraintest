//
//  HelloWorldScene.h
//  TerrainTest
//
//  Created by Jeon ByungSoo on 13. 6. 4..
//  Copyright __MyCompanyName__ 2013년. All rights reserved.
//
#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

// When you import this file, you import all the cocos2d classes
#include "cocos2d.h"
#include "Box2D.h"
#include "GLES-Render.h"
#include "Constant.h"

using namespace cocos2d;
using namespace std;

class WaterWave;

class HelloWorld : public cocos2d::CCLayer {
public:
    ~HelloWorld();
    HelloWorld();
    static cocos2d::CCScene* scene();

    CREATE_FUNC(HelloWorld);
    bool init();
    
//    virtual void draw();
    virtual void update(float dt);
    WaterWave *m_water;
    CCSprite *m_object;
};

#endif // __HELLO_WORLD_H__
