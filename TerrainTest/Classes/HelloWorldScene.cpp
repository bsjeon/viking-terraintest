//
//  HelloWorldScene.cpp
//  TerrainTest
//
//  Created by Jeon ByungSoo on 13. 6. 4..
//  Copyright __MyCompanyName__ 2013년. All rights reserved.
//
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "WaterWave.h"

using namespace CocosDenshion;

CCScene* HelloWorld::scene()
{
    CCScene *scene = CCScene::create();
    
    HelloWorld* layer = HelloWorld::create();
        
    scene->addChild(layer);

    return scene;
}

HelloWorld::HelloWorld()
{

}

HelloWorld::~HelloWorld()
{
    
}

bool HelloWorld::init()
{
    if (!CCLayer::init()) {
        return false;
    }
    
    CCRect bounds = CCRectMake(0, 0, 480, 160);
    m_water = WaterWave::create(bounds, 48, 0.999, 0.97);
    m_water->setUserData((void *) 0);
    this->addChild(m_water);
    
    m_object = CCSprite::create("CloseNormal.png");
    m_object->setPosition(ccp(SCREEN_W/2, SCREEN_H));
    this->addChild(m_object,2);
    
    CCMoveTo *moveDown = CCMoveTo::create(1.0f, ccp(SCREEN_W/2,0));
    m_object->runAction(moveDown);
    
    scheduleUpdate();
    return true;
}

void HelloWorld::update(float dt)
{
    vector<ccVertex2F>* surface = m_water->getSurfaceVec();
    
    
    for (int i=0; i<surface->size(); ++i) {
        if (m_object->boundingBox().getMinX() < surface->at(i).x &&
            surface->at(i).x < m_object->boundingBox().getMaxX()) {
            
            if (m_water->getUserData() != (void *) 1
                && surface->at(i).y > m_object->boundingBox().getMinY()) {
                m_water->makeSplashAt(m_object->getPositionX());
                m_water->setUserData((void *) 1);
                break;
            }
        }
    }
}

//void HelloWorld::draw()
//{
//    setShaderProgram(m_program);
//    getShaderProgram()->use();
//    
//    CC_NODE_DRAW_SETUP();
//    
//    updateTexCoords2();
//    
////    ccGLBlendFunc(CC_BLEND_SRC, GL_ZERO);
//
//    glEnableVertexAttribArray(kVertexPosition_Idx);
//    glEnableVertexAttribArray(kTexCoord0_Idx);
//    glEnableVertexAttribArray(kTexCoord1_Idx);
//
////    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords);
//
//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, m_terrain->getTexture()->getName());
//    glUniform1i(m_texture0ID, 0);
//    
//    glActiveTexture(GL_TEXTURE1);
//    glBindTexture(GL_TEXTURE_2D, m_terrain2->getTexture()->getName());
//    glUniform1i(m_texture1ID, 1);
//    
//    glVertexAttribPointer(kVertexPosition_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &m_borderVs[0]);
//    glVertexAttribPointer(kTexCoord0_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &m_texCoords[0]);
//    glVertexAttribPointer(kTexCoord1_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &m_texCoords2[0]);
//    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei) m_borderVs.size());
//
//    CC_INCREMENT_GL_DRAWS(1);
//}

