uniform sampler2D Texture0;
uniform sampler2D Texture1;

varying vec2 va_texCoord;
varying vec2 vb_texCoord;

void main()
{
    vec4 texelA = texture2D(Texture0, va_texCoord);
    vec4 texelB = texture2D(Texture1, vb_texCoord);
    gl_FragColor = (1.0 - texelB.a)*texelA+texelB;
}