//
//  WaterWave.h
//  TerrainTest
//
//  Created by Jeon ByungSoo on 13. 6. 7..
//
//

#ifndef __TerrainTest__WaterWave__
#define __TerrainTest__WaterWave__

#include "cocos2d.h"
#include "Constant.h"

#define TextureSize 512

#define kVertexPosition "a_position"
#define kTexCoord0 "a_texCoord"
#define kTexCoord1 "b_texCoord"
#define kTexSample0 "Texture0"
#define kTexSample1 "Texture1"

enum {
    kVertexPosition_Idx,
    kTexCoord0_Idx,
    kTexCoord1_Idx,
};

class WaterWave : public cocos2d::CCNode
{
public:
    WaterWave();
    ~WaterWave();
    
    bool initWithParams(cocos2d::CCRect bounds, int nVerts,
                        float damping, float diffusion);
    static WaterWave* create(cocos2d::CCRect bounds, int nVerts,
                            float damping, float diffusion);
    virtual void draw();
    virtual void update(float dt);

    void makeSplashAt(float x);
    void updateTexCoords2();
    std::vector<cocos2d::ccVertex2F>* getSurfaceVec();
    
private:
    cocos2d::CCRect m_bounds;
    
    float m_damping;
    float m_diffusion;

    int m_nVerts;
    std::vector<float> m_h1;
    std::vector<float> m_h2;
    
    void vertlet();
    
    inline float diffuse(float diff, float damp, float prev, float curr, float next);
    void diffuse();
    
    float dx();

    /**
     Ported from HelloWorld
     **/
    /////////////////////////////////////////////////////////
    
    std::vector<cocos2d::ccVertex2F> m_texCoords;
    std::vector<cocos2d::ccVertex2F> m_texCoords2;
    std::vector<cocos2d::ccVertex2F> m_surfaceVerts;
    
    cocos2d::CCSprite *m_terrain;
    cocos2d::CCSprite *m_terrain2;
    cocos2d::CCGLProgram *m_program;
    GLuint m_texture0ID;
    GLuint m_texture1ID;
    
    float m_cumulatedT;
};

#endif /* defined(__TerrainTest__WaterWave__) */
