//
//  Waves1DNode.m
//  TerrainTest
//
//  Created by Jeon ByungSoo on 13. 6. 7..
//
//

#import "Waves1DNode.h"

static CCGLProgram *shader_ = nil;

@implementation Waves1DNode

@synthesize speed = _speed;
@synthesize water = _water;

-(id)initWithBounds:(CGRect)bounds count:(int)count damping:(float)damping diffusion:(float)diffusion;
{
    if((self = [super init])){
        _bounds = bounds;
        _count = count;
        _damping = damping;
        _diffusion = diffusion;
        
        _h1 = calloc(_count, sizeof(float));
        _h2 = calloc(_count, sizeof(float));
        
        _speed = 0;
        
        offset = 0;
        
        shader_ = [[CCGLProgram alloc] initWithVertexShaderFilename:@"PositionTexture.vsh"
                                             fragmentShaderFilename:@"PositionTexture.fsh"];
        [shader_ addAttribute:kCCAttributeNamePosition index:kCCVertexAttrib_Position];
        [shader_ addAttribute:kCCAttributeNameTexCoord index:kCCVertexAttrib_TexCoords];
        [shader_ link];
        [shader_ updateUniforms];
        
        _textureLocation = glGetUniformLocation( shader_->program_, "u_texture");
        
#ifndef DRAW_BOX2D_WORLD
        textureSize = 256*CC_CONTENT_SCALE_FACTOR();
        
        self.water = [CCSprite spriteWithFile:@"Water.png"];
        
        ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_CLAMP_TO_EDGE};
        [self.water.texture setTexParameters:&tp];
        
#endif
    }
    
    return self;
}

- (void) dealloc
{
    free(_h1);
    free(_h2);
    
#ifndef DRAW_BOX2D_WORLD
    self.water = nil;
#endif
    
    [super dealloc];
}

-(void)vertlet {
    offset -= 0.1f;
    
    for(int i=0; i<_count; i++) _h1 = 2.0*_h2 - _h1;
    
    float *temp = _h2;
    _h2 = _h1;
    _h1 = temp;
}

static inline float
diffuse(float diff, float damp, float prev, float curr, float next){
    return (curr*diff + ((prev + next)*0.5f)*(1.0f - diff))*damp;
}

-(void)diffuse {
    float prev = _h2[0];
    float curr = _h2[0];
    float next = _h2[1];
    
    _h2[0] = diffuse(_diffusion, _damping, prev, curr, next);
    
    for(int i=1; i<(_count - 1); ++i){
        prev = curr;
        curr = next;
        next = _h2;
        
        _h2 = diffuse(_diffusion, _damping, prev, curr, next);
    }
    
    prev = curr;
    curr = next;
    _h2[_count - 1] = diffuse(_diffusion, _damping, prev, curr, next);
}

-(float)dx{return _bounds.size.width/(GLfloat)(_count - 1);}

- (void)draw {
    
    // It would be better to run these on a fixed timestep.
    // As an GFX only effect it doesn't really matter though.
    [self vertlet];
    [self diffuse];
    
    GLfloat dx = [self dx];
    GLfloat top = _bounds.size.height;
    
    // Build a vertex array and render it.
    struct Vertex{GLfloat x,y;};
    struct Vertex verts[_count*2];
    struct TexVertex{GLfloat x,y;};
    struct TexVertex texVerts[_count*2];
    for(int i=0; i<_count; i++){
        GLfloat x = i*dx;
        verts[2*i + 0] = (struct Vertex){x, (top + _h2)};
        verts[2*i + 1] = (struct Vertex){x, (top + _h2)-(float)textureSize};
        texVerts[2*i + 0] = (struct TexVertex){(x-offset)/(float)textureSize, 0};
        texVerts[2*i + 1] = (struct TexVertex){(x-offset)/(float)textureSize, 1.0f};
    }
    
    ccGLBlendFunc( CC_BLEND_SRC, CC_BLEND_DST );
    
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords );
    ccGLUseProgram( shader_->program_ );
    ccGLUniformModelViewProjectionMatrix( shader_ );
    
    ccGLBindTexture2D([self.water.texture name]);
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, verts);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, texVerts);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei) _count*2);
    
}

-(void)makeSplashAt:(float)x;
{
    // Changing the values of heightfield in h2 will make the waves move.
    // Here I only change one column, but you get the idea.
    // Change a bunch of the heights using a nice smoothing function for a better effect.
    
    int index = MAX(0, MIN((int)(x/[self dx]), _count - 1));
    _h2[index] += _speed;
    
}

@end